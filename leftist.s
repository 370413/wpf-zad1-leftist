	.section .rodata.cst8,"a",@progbits
	.align	16
caml_negf_mask:	.quad   0x8000000000000000, 0
	.align	16
caml_absf_mask:	.quad   0x7FFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF
	.data
	.globl	camlLeftist__data_begin
camlLeftist__data_begin:
	.text
	.globl	camlLeftist__code_begin
camlLeftist__code_begin:
	.data
	.quad	11008
	.globl	camlLeftist
camlLeftist:
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.quad	1
	.data
	.quad	3063
camlLeftist__2:
	.quad	camlLeftist__delete_min_1046
	.quad	3
	.data
	.quad	4087
camlLeftist__3:
	.quad	caml_curry2
	.quad	5
	.quad	camlLeftist__add_1043
	.data
	.quad	4087
camlLeftist__4:
	.quad	caml_curry2
	.quad	5
	.quad	camlLeftist__join_1039
	.data
	.quad	3063
camlLeftist__5:
	.quad	camlLeftist__depth_1033
	.quad	3
	.data
	.quad	3063
camlLeftist__6:
	.quad	camlLeftist__right_1027
	.quad	3
	.data
	.quad	3063
camlLeftist__7:
	.quad	camlLeftist__root_1021
	.quad	3
	.data
	.quad	3063
camlLeftist__8:
	.quad	camlLeftist__left_1015
	.quad	3
	.data
	.quad	3063
camlLeftist__9:
	.quad	camlLeftist__is_empty_1013
	.quad	3
	.data
	.quad	3068
camlLeftist__1:
	.ascii	"Leftist.Empty"
	.space	2
	.byte	2
	.text
	.align	16
	.globl	camlLeftist__is_empty_1013
camlLeftist__is_empty_1013:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset	8
.L100:
	movq	$1, %rsi
	movq	%rax, %rdi
	movq	caml_equal@GOTPCREL(%rip), %rax
	call	caml_c_call@PLT
.L101:
	movq	caml_young_ptr@GOTPCREL(%rip), %r11
	movq    (%r11), %r15
	addq	$8, %rsp
	.cfi_adjust_cfa_offset	-8
	ret
	.cfi_adjust_cfa_offset	8
	.cfi_endproc
	.type	camlLeftist__is_empty_1013,@function
	.size	camlLeftist__is_empty_1013,.-camlLeftist__is_empty_1013
	.text
	.align	16
	.globl	camlLeftist__left_1015
camlLeftist__left_1015:
	.cfi_startproc
.L103:
	cmpq	$1, %rax
	je	.L102
	movq	(%rax), %rax
	ret
	.align	4
.L102:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.cfi_endproc
	.type	camlLeftist__left_1015,@function
	.size	camlLeftist__left_1015,.-camlLeftist__left_1015
	.text
	.align	16
	.globl	camlLeftist__root_1021
camlLeftist__root_1021:
	.cfi_startproc
.L105:
	cmpq	$1, %rax
	je	.L104
	movq	8(%rax), %rax
	ret
	.align	4
.L104:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.cfi_endproc
	.type	camlLeftist__root_1021,@function
	.size	camlLeftist__root_1021,.-camlLeftist__root_1021
	.text
	.align	16
	.globl	camlLeftist__right_1027
camlLeftist__right_1027:
	.cfi_startproc
.L107:
	cmpq	$1, %rax
	je	.L106
	movq	16(%rax), %rax
	ret
	.align	4
.L106:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.cfi_endproc
	.type	camlLeftist__right_1027,@function
	.size	camlLeftist__right_1027,.-camlLeftist__right_1027
	.text
	.align	16
	.globl	camlLeftist__depth_1033
camlLeftist__depth_1033:
	.cfi_startproc
.L109:
	cmpq	$1, %rax
	je	.L108
	movq	24(%rax), %rax
	ret
	.align	4
.L108:
	movq	$1, %rax
	ret
	.cfi_endproc
	.type	camlLeftist__depth_1033,@function
	.size	camlLeftist__depth_1033,.-camlLeftist__depth_1033
	.text
	.align	16
	.globl	camlLeftist__join_1039
camlLeftist__join_1039:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_adjust_cfa_offset	24
.L140:
	movq	%rax, 8(%rsp)
	movq	%rbx, %rax
	movq	%rax, 0(%rsp)
	call	camlLeftist__is_empty_1013@PLT
.L141:
	cmpq	$1, %rax
	je	.L139
	movq	8(%rsp), %rax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset	-24
	ret
	.cfi_adjust_cfa_offset	24
	.align	4
.L139:
	movq	8(%rsp), %rax
	call	camlLeftist__is_empty_1013@PLT
.L142:
	cmpq	$1, %rax
	je	.L138
	movq	0(%rsp), %rax
	addq	$24, %rsp
	.cfi_adjust_cfa_offset	-24
	ret
	.cfi_adjust_cfa_offset	24
	.align	4
.L138:
	movq	0(%rsp), %rax
	cmpq	$1, %rax
	je	.L137
	movq	8(%rax), %rsi
	jmp	.L136
	.align	4
.L137:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L136:
	movq	8(%rsp), %rbx
	cmpq	$1, %rbx
	je	.L135
	movq	%rax, 0(%rsp)
	movq	%rbx, 8(%rsp)
	movq	8(%rbx), %rdi
	jmp	.L134
	.align	4
.L135:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L134:
	movq	caml_greaterthan@GOTPCREL(%rip), %rax
	call	caml_c_call@PLT
.L143:
	movq	caml_young_ptr@GOTPCREL(%rip), %r11
	movq    (%r11), %r15
	cmpq	$1, %rax
	je	.L133
	movq	0(%rsp), %rax
	movq	8(%rsp), %rbx
	jmp	.L140
	.align	4
.L133:
	movq	8(%rsp), %rax
	cmpq	$1, %rax
	je	.L132
	movq	%rax, 8(%rsp)
	movq	16(%rax), %rax
	jmp	.L131
	.align	4
.L132:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L131:
	movq	0(%rsp), %rbx
	call	camlLeftist__join_1039@PLT
.L144:
	movq	%rax, %rbx
	cmpq	$1, %rbx
	je	.L130
	movq	24(%rbx), %rdi
	jmp	.L129
	.align	4
.L130:
	movq	$1, %rax
	movq	%rax, %rdi
.L129:
	movq	8(%rsp), %rax
	cmpq	$1, %rax
	je	.L128
	movq	(%rax), %rsi
	jmp	.L127
	.align	4
.L128:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L127:
	cmpq	$1, %rsi
	je	.L126
	movq	24(%rsi), %rsi
	jmp	.L125
	.align	4
.L126:
	movq	$1, %rsi
.L125:
	cmpq	%rdi, %rsi
	jle	.L118
	cmpq	$1, %rbx
	je	.L124
	movq	24(%rbx), %rdi
	jmp	.L123
	.align	4
.L124:
	movq	$1, %rdi
.L123:
	addq	$2, %rdi
	cmpq	$1, %rax
	je	.L122
	movq	8(%rax), %rsi
	jmp	.L121
	.align	4
.L122:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L121:
	cmpq	$1, %rax
	je	.L120
	movq	(%rax), %rdx
	jmp	.L119
	.align	4
.L120:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L119:
.L145:	subq	$40, %r15
	movq	caml_young_limit@GOTPCREL(%rip), %rax
	cmpq	(%rax), %r15
	jb	.L146
	leaq	8(%r15), %rax
	movq	$4096, -8(%rax)
	movq	%rdx, (%rax)
	movq	%rsi, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rdi, 24(%rax)
	addq	$24, %rsp
	.cfi_adjust_cfa_offset	-24
	ret
	.cfi_adjust_cfa_offset	24
	.align	4
.L118:
	cmpq	$1, %rax
	je	.L117
	movq	(%rax), %rdi
	jmp	.L116
	.align	4
.L117:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L116:
	cmpq	$1, %rdi
	je	.L115
	movq	24(%rdi), %rdi
	jmp	.L114
	.align	4
.L115:
	movq	$1, %rdi
.L114:
	addq	$2, %rdi
	cmpq	$1, %rax
	je	.L113
	movq	(%rax), %rsi
	jmp	.L112
	.align	4
.L113:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L112:
	cmpq	$1, %rax
	je	.L111
	movq	8(%rax), %rdx
	jmp	.L110
	.align	4
.L111:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L110:
.L148:	subq	$40, %r15
	movq	caml_young_limit@GOTPCREL(%rip), %rax
	cmpq	(%rax), %r15
	jb	.L149
	leaq	8(%r15), %rax
	movq	$4096, -8(%rax)
	movq	%rbx, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rsi, 16(%rax)
	movq	%rdi, 24(%rax)
	addq	$24, %rsp
	.cfi_adjust_cfa_offset	-24
	ret
	.cfi_adjust_cfa_offset	24
.L149:	call	caml_call_gc@PLT
.L150:	jmp	.L148
.L146:	call	caml_call_gc@PLT
.L147:	jmp	.L145
	.cfi_endproc
	.type	camlLeftist__join_1039,@function
	.size	camlLeftist__join_1039,.-camlLeftist__join_1039
	.text
	.align	16
	.globl	camlLeftist__add_1043
camlLeftist__add_1043:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset	8
.L151:
	movq	%rax, %rdi
.L152:	subq	$40, %r15
	movq	caml_young_limit@GOTPCREL(%rip), %rax
	cmpq	(%rax), %r15
	jb	.L153
	leaq	8(%r15), %rax
	movq	$4096, -8(%rax)
	movq	$1, (%rax)
	movq	%rdi, 8(%rax)
	movq	$1, 16(%rax)
	movq	$3, 24(%rax)
	addq	$8, %rsp
	.cfi_adjust_cfa_offset	-8
	jmp	camlLeftist__join_1039@PLT
	.cfi_adjust_cfa_offset	8
.L153:	call	caml_call_gc@PLT
.L154:	jmp	.L152
	.cfi_endproc
	.type	camlLeftist__add_1043,@function
	.size	camlLeftist__add_1043,.-camlLeftist__add_1043
	.text
	.align	16
	.globl	camlLeftist__delete_min_1046
camlLeftist__delete_min_1046:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset	8
.L161:
	cmpq	$1, %rax
	je	.L160
	movq	16(%rax), %rbx
	jmp	.L159
	.align	4
.L160:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L159:
	cmpq	$1, %rax
	je	.L158
	movq	%rax, 0(%rsp)
	movq	(%rax), %rax
	jmp	.L157
	.align	4
.L158:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L157:
	call	camlLeftist__join_1039@PLT
.L162:
	movq	%rax, %rbx
	movq	0(%rsp), %rax
	cmpq	$1, %rax
	je	.L156
	movq	8(%rax), %rdi
	jmp	.L155
	.align	4
.L156:
	movq	camlLeftist@GOTPCREL(%rip), %rax
	movq	16(%rax), %rax
	movq	%r14, %rsp
	popq	%r14
	ret
	.align	4
.L155:
.L163:	subq	$24, %r15
	movq	caml_young_limit@GOTPCREL(%rip), %rax
	cmpq	(%rax), %r15
	jb	.L164
	leaq	8(%r15), %rax
	movq	$2048, -8(%rax)
	movq	%rdi, (%rax)
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	.cfi_adjust_cfa_offset	-8
	ret
	.cfi_adjust_cfa_offset	8
.L164:	call	caml_call_gc@PLT
.L165:	jmp	.L163
	.cfi_endproc
	.type	camlLeftist__delete_min_1046,@function
	.size	camlLeftist__delete_min_1046,.-camlLeftist__delete_min_1046
	.text
	.align	16
	.globl	camlLeftist__entry
camlLeftist__entry:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_adjust_cfa_offset	8
.L166:
	call	caml_alloc2@PLT
.L167:
	leaq	8(%r15), %rdi
	movq	$2296, -8(%rdi)
	movq	camlLeftist__1@GOTPCREL(%rip), %rax
	movq	%rax, (%rdi)
	movq	$1, 8(%rdi)
	call	caml_set_oo_id@PLT
	movq	camlLeftist@GOTPCREL(%rip), %rbx
	movq	%rax, 16(%rbx)
	movq	$1, (%rbx)
	movq	camlLeftist__9@GOTPCREL(%rip), %rax
	movq	%rax, 40(%rbx)
	movq	camlLeftist__8@GOTPCREL(%rip), %rax
	movq	%rax, 48(%rbx)
	movq	camlLeftist__7@GOTPCREL(%rip), %rax
	movq	%rax, 56(%rbx)
	movq	camlLeftist__6@GOTPCREL(%rip), %rax
	movq	%rax, 64(%rbx)
	movq	camlLeftist__5@GOTPCREL(%rip), %rax
	movq	%rax, 72(%rbx)
	movq	camlLeftist__4@GOTPCREL(%rip), %rax
	movq	%rax, 32(%rbx)
	movq	camlLeftist__3@GOTPCREL(%rip), %rax
	movq	%rax, 8(%rbx)
	movq	camlLeftist__2@GOTPCREL(%rip), %rax
	movq	%rax, 24(%rbx)
	movq	$1, %rax
	addq	$8, %rsp
	.cfi_adjust_cfa_offset	-8
	ret
	.cfi_adjust_cfa_offset	8
	.cfi_endproc
	.type	camlLeftist__entry,@function
	.size	camlLeftist__entry,.-camlLeftist__entry
	.data
	.text
	.globl	camlLeftist__code_end
camlLeftist__code_end:
	.data
	.globl	camlLeftist__data_end
camlLeftist__data_end:
	.long	0
	.globl	camlLeftist__frametable
camlLeftist__frametable:
	.quad	11
	.quad	.L167
	.word	16
	.word	0
	.align	8
	.quad	.L165
	.word	16
	.word	2
	.word	5
	.word	3
	.align	8
	.quad	.L162
	.word	16
	.word	1
	.word	0
	.align	8
	.quad	.L154
	.word	16
	.word	2
	.word	3
	.word	5
	.align	8
	.quad	.L150
	.word	32
	.word	3
	.word	9
	.word	7
	.word	3
	.align	8
	.quad	.L147
	.word	32
	.word	3
	.word	9
	.word	7
	.word	3
	.align	8
	.quad	.L144
	.word	32
	.word	1
	.word	8
	.align	8
	.quad	.L143
	.word	32
	.word	2
	.word	0
	.word	8
	.align	8
	.quad	.L142
	.word	32
	.word	2
	.word	0
	.word	8
	.align	8
	.quad	.L141
	.word	32
	.word	2
	.word	0
	.word	8
	.align	8
	.quad	.L101
	.word	16
	.word	0
	.align	8
	.section .note.GNU-stack,"",%progbits
