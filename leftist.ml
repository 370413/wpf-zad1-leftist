(* Author: Tomasz Necio
   Code review: Mateusz Bajorek *)

(* queue is a leftist tree; a leaf is a Queue with 2 EmptyQueues and a value;
   every non-empty node also contains depth (int) which is number of non-empty
   nodes in its rightmost path *)
type 'a queue = EmptyQueue | Queue of 'a queue * 'a * 'a queue * int

(* exception for attempts to take a subtree or root of an empty queue *)
exception Empty

let empty = EmptyQueue

let is_empty q = (q = empty)

let left t =
    match t with
    | Queue (l, a, r, d) -> l
    | EmptyQueue -> raise Empty

let root t =
    match t with
    | Queue (l, a, r, d) -> a
    | EmptyQueue -> raise Empty

let right t =
    match t with
    | Queue (l, a, r, d) -> r
    | EmptyQueue -> raise Empty

let depth t =
    match t with
    | Queue (l, a, r, d) -> d
    | EmptyQueue -> 0

let rec join d1 d2 =
    (* adding empty queue doesn't change anything;
       we put this inside join becuase this is the place where join returns *)
    if is_empty d2 then d1 else
    if is_empty d1 then d2 else
    (* root should always be no bigger than anything below it
       so the tree with bigger root is joined with one of the subtrees *)
    if root d1 > root d2 then join d2 d1 else
        (* we join right subtree of d1 with d2 and then place it so that
           the tree is still leftist *)
        let new_queue = (join (right d1) d2) in
        if depth (left d1) > depth new_queue then
            (* depth of a tree is always 1 + depth of its right subtree *)
            Queue ((left d1), (root d1), new_queue, ((depth new_queue) + 1))
        else
            Queue (new_queue, (root d1), (left d1), ((depth (left d1)) + 1))

let add element q =
    join (Queue (empty, element, empty, 1)) q

let delete_min q =
    (root q, join (left q) (right q))
