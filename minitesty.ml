open Leftist;;
let a = empty;;
assert(is_empty a);;
let b = add 1 a;;
assert((fst (delete_min b) = 1));;
let a = add 3 a;;
let a = add 15 a;;
let a = add 151 a;;
let a = add 251 a;;
let a = add (-9) a;;
let b = add 13 empty;;
let c = add 199 empty;;
let c = add 231 c;;
let d = join b c;;
let e = join d a;;
let rec aux l acc q = if is_empty q then l else aux ((fst (delete_min q))::l) (acc - 1) (snd (delete_min q)) in aux [] 100 e;;

